(ns scrum-tools.domain.story-test
  (:require [scrum-tools.domain.story :as s]
            [clojure.test :refer :all]))

(defn- gen-story []
  {:id "id"
   :title "title"
   :owner "owner"
   :storypoints 1
   :link "link"})

(defn- story-with [param value]
  (assoc (gen-story) param value))

(deftest story-spec-validity
  (testing "test input is valid"
    (is (s/valid? (gen-story))))

  (testing "string fields are validated"
    (is (not (s/valid? (story-with :id 1))))
    (is (not (s/valid? (story-with :id ""))))

    (is (not (s/valid? (story-with :title 1))))
    (is (not (s/valid? (story-with :title ""))))

    (is (not (s/valid? (story-with :owner 1))))
    (is (not (s/valid? (story-with :owner ""))))

    (is (not (s/valid? (story-with :link 1))))
    (is (not (s/valid? (story-with :link "")))))

  (testing "numeric fields are validated"
    (is (not (s/valid? (story-with :storypoints 0))))
    (is (not (s/valid? (story-with :storypoints -2))))
    (is (thrown? ClassCastException (s/valid? (story-with :storypoints ""))))))

(deftest average-storypoints
  (testing "average calculation"
    (is (= 2 (s/avg-sp-per-story [(story-with :storypoints 2)])))
    (is (= 0.5 (s/avg-sp-per-story [(story-with :storypoints 0.5)
                                  (story-with :storypoints 0.5)])))
    (is (= 3 (s/avg-sp-per-story [(story-with :storypoints 2)
                                 (story-with :storypoints 2)
                                 (story-with :storypoints 3)
                                 (story-with :storypoints 5)])))))
