(ns scrum-tools.domain.sprint-test
  (:require [scrum-tools.domain.sprint :as sp]
            [clojure.test :refer :all]))

(defn- gen-vel-input [storypoints workdays]
  {:resolved {:storypoints storypoints :stories 1} :available-workdays workdays})

(deftest resolve-sprint
  (testing "Resolves sprint"
    (is (= {:resolved{:stories 4 :storypoints 10.5}} (sp/resolve-sprint {} {:stories 4 :storypoints 10.5}))))

  (testing "Validates input"
    (is (thrown? AssertionError (sp/resolve-sprint {} {})))
    (is (thrown? AssertionError (sp/resolve-sprint {} {:stories 4})))
    (is (thrown? AssertionError (sp/resolve-sprint {} {:stories "4.5" :storypoints 10})))
    (is (thrown? AssertionError (sp/resolve-sprint {} {:storypoints 10})))
    (is (thrown? ClassCastException (sp/resolve-sprint {} {:stories 4 :storypoints "10.5"})))))

(deftest velocity
  (testing "Velocity calculation"
    (are [x y] (= x y)
      (sp/velocity (gen-vel-input 12 30)) 12
      (sp/velocity (gen-vel-input 10 30)) 10
      (sp/velocity (gen-vel-input 123456789 30)) 123456789
      (sp/velocity (gen-vel-input 1 30)) 1)))

(deftest velocity-per-workday
  (testing "Simple success case"
    (is (= 3 (sp/velocity-per-workday (gen-vel-input 45 15)))))

  (testing "Fraction result success case"
    (is (= 45/11 (sp/velocity-per-workday (gen-vel-input 45 11)))))

  (testing "Validates input"
    (is (thrown? AssertionError (sp/velocity-per-workday {:available-workdays 15})))
    (is (thrown? AssertionError (sp/velocity-per-workday {:resolved-storypoints 45})))
    (is (thrown? AssertionError (sp/velocity-per-workday (gen-vel-input 45 0))))
    (is (thrown? AssertionError (sp/velocity-per-workday (gen-vel-input 0 14))))))


(deftest average-velocity-per-workday
  (testing "Simple success case"
    (is (= 4 (sp/average-velocity-per-workday [(gen-vel-input 50 10)
                                               (gen-vel-input 45 15)])))))
