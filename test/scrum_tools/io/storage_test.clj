(ns scrum-tools.io.storage-test
  (:require [scrum-tools.io.storage :as st]
            [clojure.test :refer :all]))

; TODO: activate when test environment is implemented
;(deftest load-teams
;  (testing "Teams are loaded from test file"
;    (let [teams (st/load-teams)]
;      (is (= 2 (count teams)))
;      (is (= "Tiger Squad" (:name (first teams)))))))
