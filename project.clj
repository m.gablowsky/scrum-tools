(defproject scrum-tools "0.1.0-SNAPSHOT"
  :description "A library providing functionality for keeping track of scrum sprints"
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/data.csv "1.0.0"]
                 [org.clojure/data.json "1.0.0"]]
  :repl-options {:init-ns scrum-tools.core})
