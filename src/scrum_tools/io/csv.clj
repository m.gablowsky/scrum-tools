(ns scrum-tools.io.csv
  (:require [scrum-tools.domain.sprint :as sp]
            [clojure.data.csv :as csv]
            [clojure.edn :as edn]
            [clojure.java.io :as io]))

(defn- read-csv [fileref]
  (with-open [reader (io/reader fileref)]
    (doall
     (csv/read-csv reader))))

(defn- edn-str [str]
  (edn/read-string str))

(defn- to-sprint [[id days committed-us committed-sp resolved-us resolved-sp available-workdays]]
  (sp/resolve-sprint (sp/vec->Sprint [id days committed-us committed-sp available-workdays])
                     {:resolved-stories (edn-str resolved-us) :resolved-storypoints (edn-str resolved-sp)}))

(defn read-sprints
  "Reads a csv file into sprint records.
  The csv values must be in this order:
  id,duration-days,committed stories,committed storypoints,resolved stories,resolved storypoints,available workdays"
  [csv-file]
  (map to-sprint (rest (read-csv csv-file))))

(defn write-csv! [data file]
  (with-open [writer (io/writer file)]
    (csv/write-csv writer data)))
