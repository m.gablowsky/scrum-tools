(ns scrum-tools.io.storage
  (:require [clojure.edn :as edn]
            [scrum-tools.domain.sprint :as sp]
            [scrum-tools.domain.team :as t]))

;; TODO: externalize config
(def base-dir "/home/marco/scrum-tools/")
(def sprints-file "sprints.edn")
(def teams-file "teams.edn")

(defn- abs-file [file]
  (str base-dir file))

(defn- to-data [file]
  (edn/read-string (slurp (abs-file file))))

(defn store-sprints! [sprints]
  (spit (abs-file sprints-file) (pr-str sprints)))

(defn load-sprints []
  (to-data sprints-file))

(defn store-teams! [teams]
  (spit (abs-file teams-file) (pr-str teams)))

(defn load-teams []
  (to-data teams-file))
