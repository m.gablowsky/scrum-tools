(ns scrum-tools.domain.sprint
  (:require [clojure.spec.alpha :as s]
            [clojure.edn :as edn]))

(s/def ::pos-include-zero (or pos? #(= 0 %)))

(s/def ::id (s/and string? not-empty))
(s/def ::duration-days pos-int?)
(s/def ::available-workdays pos?)

(s/def ::start inst?)
(s/def ::end inst?)
(s/def ::dates (s/keys :req-un [::start ::end]))

(s/def ::stories pos-int?)
(s/def ::storypoints ::pos-include-zero)
(s/def ::committed (s/keys :req-un [::stories ::storypoints]))
(s/def ::resolved (s/keys :req-un [::stories ::storypoints]))

(s/def ::sprint (s/keys :req-un [::id
                                 ::duration-days
                                 ::available-workdays
                                 ::dates
                                 ::committed]
                        :opt-un [::resolved]))

(s/def ::velocity-input (s/keys :req-un [::available-workdays ::resolved]))
(s/def ::velocity-inputs (s/coll-of ::velocity-input))

(defn resolve-sprint [sprint {:keys [stories storypoints] :as data}]
  {:pre [(s/valid? ::resolved data)]}
  (assoc sprint :resolved data))

(defn velocity [{:keys [resolved] :as sprint}]
  {:pre [(s/valid? ::resolved (:resolved sprint))]}
  (:storypoints resolved))

(defn velocity-per-workday [{:keys [resolved available-workdays] :as input}]
  {:pre [(s/valid? ::velocity-input input)]}
  (/ (:storypoints resolved) available-workdays))

(defn average-velocity-per-workday [sprints]
  {:pre [(s/valid? ::velocity-inputs sprints)]}
  (/ (reduce + (map velocity-per-workday sprints)) (count sprints)))
