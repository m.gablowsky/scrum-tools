(ns scrum-tools.domain.story
  (:require [clojure.spec.alpha :as s]))

(s/def ::non-empty-string (s/and string? not-empty))

(s/def ::id ::non-empty-string)
(s/def ::title ::non-empty-string)
(s/def ::owner ::non-empty-string)
(s/def ::storypoints pos?)
(s/def ::link ::non-empty-string)

(s/def ::story (s/keys :req-un [::id
                                ::title
                                ::owner
                                ::storypoints
                                ::link]))

(s/def ::stories (s/coll-of ::story))

(defn valid? [story]
  (s/valid? ::story story))

(defn avg-sp-per-story [stories]
  {:pre [(s/valid? ::stories stories)]}
  (/ (reduce + (map :storypoints stories)) (count stories)))
