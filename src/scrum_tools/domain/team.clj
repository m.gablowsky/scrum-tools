(ns scrum-tools.domain.team
  (:require [clojure.spec.alpha :as s]))

(s/def ::non-empty-string (s/and string? not-empty))

(s/def ::name ::non-empty-string)
(s/def ::firstname ::non-empty-string)
(s/def ::lastname ::non-empty-string)
(s/def ::email ::non-empty-string)

(s/def ::member (s/keys :req-un [::firstname ::lastname ::email]))
(s/def ::members (s/coll-of ::member))

(s/def ::team (s/keys :req-un [::name ::members]))
(s/def ::teams (s/coll-of ::team))
