(ns scrum-tools.statistics
  (:require [scrum-tools.io.jira :as j]
            [scrum-tools.io.csv :as c]
            [clojure.xml :as xml]))

(def csv-headers ["key" "summary" "priority" "reporter" "status" "resolved by" "time spent minutes"])

(defn- to-csv-row [{:keys [key summary reporter priority status time-spent-minutes resolved-by]}]
  [key summary priority reporter status resolved-by (.floatValue time-spent-minutes)])

(defn- to-csv-data [items]
  (into [] (conj (map to-csv-row items) csv-headers)))

(defn generate-test-support-stats []
  ;; FIXME: hardcoded files must be replaced
  (let [items (j/items (xml/parse "/home/marco/jira-search-result.xml"))]
    (-> (map j/collect-stats items)
        to-csv-data
        (c/write-csv! "/home/marco/scrum-tools/sprint-X-test-sup.csv"))))
